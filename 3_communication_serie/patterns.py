from machine import Pin

def green(green: Pin, yellow: Pin, red: Pin) -> None:
    green.on()
    yellow.off()
    red.off()

def yellow(green: Pin, yellow: Pin, red: Pin) -> None:
    green.off()
    yellow.on()
    red.off()

def red(green: Pin, yellow: Pin, red: Pin) -> None:
    green.off()
    yellow.off()
    red.on()

def all_on(green: Pin, yellow: Pin, red: Pin) -> None:
    green.on()
    yellow.on()
    red.on()

def all_off(green: Pin, yellow: Pin, red: Pin) -> None:
    green.off()
    yellow.off()
    red.off()
