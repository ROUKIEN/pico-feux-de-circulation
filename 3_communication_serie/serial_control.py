import sys

from machine import Pin

import uselect
from patterns import red, green, yellow

def serial_control(led_green: Pin, led_yellow: Pin, led_red: Pin) -> None:
    while True:
        if uselect.select([sys.stdin], [], [], 0)[0]:
            ch = sys.stdin.readline().strip()
            if ch == 'green':
                green(led_green, led_yellow, led_red)
            elif ch == 'yellow':
                yellow(led_green, led_yellow, led_red)
            elif ch == 'red':
                red(led_green, led_yellow, led_red)
            else:
                print('dunno what to do, got: "'+ch+'"')
    print('over')
