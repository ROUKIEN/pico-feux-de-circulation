from machine import Pin

import time

from patterns import all_on, all_off
from serial_control import *

led_red = Pin(11, Pin.OUT)
led_green = Pin(13, Pin.OUT)
led_yellow = Pin(14, Pin.OUT)

def init(green: Pin, yellow: Pin, red: Pin) -> None:
    all_off(green, yellow, red)
    time.sleep(1)
    all_on(green, yellow, red)
    time.sleep(1)
    all_off(green, yellow, red)

init(led_green, led_yellow, led_red)

serial_control(led_green, led_yellow, led_red)
