### Introduction au pi pico et micropython

Ce répertoire contient quelques exercices permettant de prendre en main le [raspberry pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) ainsi que le langage [micropython](https://micropython.org/) au travers de la réalisation d'un feu de circulation.

### Broches du Pico

![./doc/picow-pinout.svg](./doc/picow-pinout.svg)


### Liste des exercices

1. Découverte avec une boucle simple
2. Utilisation du PWM pour allumer/éteindre une LED progressivement
3. Communiquer avec le pico depuis un port série
4. Controler plusieurs pico depuis un programme externe

Reste à faire

* Ajout d'un bouton pour demander le passage au rouge du feu
