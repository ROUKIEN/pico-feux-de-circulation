from machine import Pin, PWM

import time

led_rouge = PWM(Pin(11, Pin.OUT))
led_rouge.freq(1_000)
led_verte = PWM(Pin(13, Pin.OUT))
led_verte.freq(1_000)
led_jaune = PWM(Pin(14, Pin.OUT))
led_jaune.freq(1_000)

# tout éteindre par défaut
led_rouge.duty(0)
led_verte.duty(0)
led_jaune.duty(0)

while True:
    for duty in range(65025):
        led_rouge.duty_u16(duty)
        sleep(0.0001)
    for duty in range(65025, 0, -1):
        led_rouge.duty_u16(duty)
        sleep(0.0001)

    for duty in range(65025):
        led_jaune.duty_u16(duty)
        sleep(0.0001)
    for duty in range(65025, 0, -1):
        led_jaune.duty_u16(duty)
        sleep(0.0001)

    for duty in range(65025):
        led_verte.duty_u16(duty)
        sleep(0.0001)
    for duty in range(65025, 0, -1):
        led_verte.duty_u16(duty)
        sleep(0.0001)
