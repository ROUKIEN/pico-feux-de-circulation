from machine import Pin, PWM

import time

led_rouge = PWM(Pin(11, Pin.OUT))
led_rouge.freq(1_000)
led_verte = PWM(Pin(13, Pin.OUT))
led_verte.freq(1_000)
led_jaune = PWM(Pin(14, Pin.OUT))
led_jaune.freq(1_000)

# tout éteindre par défaut
led_rouge.duty_u16(0)
led_verte.duty_u16(0)
led_jaune.duty_u16(0)

while True:
    # il est possible de simplifier notre code en créant une liste de nos LEDs
    for led in [led_rouge, led_verte, led_jaune]:
        for duty in range(65025):
            led.duty_u16(duty)
            time.sleep(0.0001)
        for duty in range(65025, 0, -1):
            led.duty_u16(duty)
            time.sleep(0.0001)
