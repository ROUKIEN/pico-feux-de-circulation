### Préparation

1. installer thonny IDE
2. injecter le .uf2 permettant d'exécuter du micropython sur le pico
3. Prendre en main thonny IDE


#### Installation de micropython sur le pi pico

1. Branchez le pi pico à l'un des ports USB de votre ordinateur **tout en appuyant sur le bouton "BOOTSEL"** présent sur la carte.

Depuis thonny IDE:

2. `Outils` -> `Options`
3. Sélectionnez l'onglet `Interpréteur` (`interpreter` en anglais)
4. En bas à droite, `Installer ou mettre à jour Micropython`
5. Sélectionnez le modèle de carte. Dans notre cas, "Raspberry Pico"

![./install_micropython_thonny_1.png](./install_micropython_thonny_1.png)

Une fois installé, débranchez et rebranchez le pi pico. Redémarrez thonny IDE.

En bas à droite, vérifiez que le pico est bien détecté (`Board in FS mode@/dev/ttyACM0`).

> Le texte affiché peut varier en fonction de votre système d'exploitation.

#### Introduction à la console

Dans la console, exécutez

* `1 + 3`
* `print('bonjour')`

Nous pourrons également exécuter des programmes plus complexes depuis la console à l'aide de `exec(open('main.py').read()) `
