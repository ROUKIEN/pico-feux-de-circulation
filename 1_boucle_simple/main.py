from machine import Pin

import time

led_rouge = Pin(11, Pin.OUT)
led_verte = Pin(13, Pin.OUT)
led_jaune = Pin(14, Pin.OUT)

# tout éteindre par défaut
led_verte.off()
led_jaune.off()
led_rouge.off()

while True:
    led_jaune.off()
    led_rouge.on()

    time.sleep(3)

    led_rouge.off()
    led_verte.on()

    time.sleep(8)

    led_verte.off()
    led_jaune.on()

    time.sleep(2)
