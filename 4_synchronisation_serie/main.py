import sys
import serial
import time
from serial.serialutil import SerialException
from traffic_light import SerialTrafficLight, Simple2Lights

try:
    ser1 = serial.Serial('/dev/ttyACM0', 115200, timeout=1)
    tf1 = SerialTrafficLight(ser1, 'green')

    ser2 = serial.Serial('/dev/ttyACM1', 115200, timeout=1)
    tf2 = SerialTrafficLight(ser2, 'red')

    s2l = Simple2Lights(tf1, tf2, 5, 2, 1)

    s2l.execute()

    ser1.close()
    ser2.close()
    print('done')
except (FileNotFoundError, SerialException) as err:
    print('oops: '+str(err))
    sys.exit(2)
