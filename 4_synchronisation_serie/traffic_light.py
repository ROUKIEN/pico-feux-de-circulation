import serial
import time
from threading import Timer

class SerialTrafficLight:
    colors = ['green', 'yellow', 'red']

    def __init__(self, serial: serial.Serial, color):
        self.serial = serial
        self.color = color
        self.setColor(color)

    def setColor(self, color):
        cmd = f'{color}\n'
        self.serial.write(bytes(cmd.encode('utf-8')))
        self.serial.flush()

    def setGreen(self):
        self.setColor('green')

    def setYellow(self):
        self.setColor('yellow')

    def setRed(self):
        self.setColor('red')

    def nextState(self):
        idx = self.colors.index(self.color)
        if idx == 2:
            idx = 0
        else:
            idx +=1
        self.color = self.colors[idx]
        self.setColor(self.colors[idx])

class Simple2Lights:
    def __init__(self, tf1: SerialTrafficLight, tf2: SerialTrafficLight, greenTime: int, yellowTime: int, securityTime: int):
        self.tf1 = tf1
        self.tf2 = tf2
        self.greenTime = greenTime
        self.yellowTime = yellowTime
        self.securityTime = securityTime

    def execute(self):
        currTf = self.tf1
        while True:
            time.sleep(self.greenTime)
            currTf.setYellow()
            time.sleep(self.yellowTime)
            currTf.setRed()
            time.sleep(self.securityTime)
            if currTf == self.tf1:
                currTf = self.tf2
            else:
                currTf = self.tf1
            currTf.setGreen()

# when a tf is red, the other must be green, yellow or red.
#
# when a tf enters the green state, we wait for its green timer to complete. when the green timer complete, we pass it to yellow.
# when a tf enters the yellow state, we wait for its yellow timer to complete. when the yellow timer complete, we pass it to red.
# when a tf enters the red state, we wait for its security timer to complete. When the security timer complete, we pass the other tf to green.
